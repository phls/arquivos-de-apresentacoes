Repositório dos arquivos usados nas minhas apresentações/palestras em divesos
eventos ao longo dos anos.

Estes arquivos estão licenciado sob uma licença Creative Commons

Atribuição-Compartilhamento pela mesma licença 4.0

<http://creativecommons.org/licenses/by-sa/4.0/deed.pt_BR>

## Videos de algumas palestras e paineis

O Projeto Debian quer você!  
DebConf19  
<https://www.youtube.com/watch?v=M8DAEqVpmzw>

Geographical Diversity BoF  
DebCnf19  
<https://www.youtube.com/watch?v=w6AR8Go1ATk>

Let's go to DebConf19 in Brazil!  
MiniDebConf Hamburgo 2019  
<https://www.youtube.com/watch?v=v39fO-5qg04>

Abertura da MiniDebConf Curitiba 2018  
MiniDebConf Curitiba 2018  
<https://www.youtube.com/watch?v=V209L07y2lw>

DebConf19: Curitiba  
DebConf18  
<https://www.youtube.com/watch?v=rQCjWvBcujU>

O Projeto Debian quer você!  
Campus Party Bahia (CPBA) 2017  
<https://www.youtube.com/watch?v=JY78vCbp2go>

O Projeto Debian quer você!  
Campus Party Brasília (CPBSB) 2017  
<https://www.youtube.com/watch?v=G26PscloNCs>

Painel: Debian Teams   
MiniDebConf Curitiba 2017  
<https://www.youtube.com/watch?v=SGLf1yHgNno>

Painel: Eventos Debian Brasil em 2017  
MiniDebConf Curitiba 2017  
<https://www.youtube.com/watch?v=Z0k43vUishs>

Abertura da MiniDebConf Curitiba 2017  
MiniDebConf Curitiba 2017  
<https://www.youtube.com/watch?v=48jVIX6sgOo>

Redes sociais livres e federadas como alternativas às redes privadas e espionadas  
Campus Party Minas Gerais (CPMG) 2016  
<https://www.youtube.com/watch?v=kcLTWlw_FOc>

Redes socias livres e federadas como alternativas as redes privadas e espionadas  
Fórum Internacional Software Livre (FISL) 2016  
<http://hemingway.softwarelivre.org/fisl17/41e/sala41e-high-201607161501.ogv>

Painel: O movimento software livre como agente transformador da sociedade   
Campus Party Brasil (CPBR) 2016  
<https://www.youtube.com/watch?v=JQOzRhagKb0&feature=youtu.be&t=6h6m>

Painel o processo de construção colaborativo de revistas eletrônicas com SL  
Fórum Internacional Software Livre (FISL) 2015  
<http://hemingway.softwarelivre.org/fisl16/high/41c/sala_41c-high-201507091602.ogv>

Redes sociais privadas e a concentração de informação: como elas afetam o SL?  
Fórum Internacional Software Livre (FISL) 2015  
<http://hemingway.softwarelivre.org/fisl16/high/41b/sala_41b-high-201507091810.ogv>

Painel: Existe Movimento Software Livre x Open Source Initiative, ou somos todos iguais?  
Campus Party Brasil (CPBR) 2015  
<https://www.youtube.com/watch?v=jz7h05l7mRU>

Painel: Mídias sociais livres como alternativas as redes devassas.  
Campus Party Brasil (CPBR) 2015  
<https://www.youtube.com/watch?v=4KXLeqVP8fA>

Painel: Você sabe o quanto é espionado? E como evitar?  
Campus Party Recife (CPRecife) 2014  
<https://www.youtube.com/watch?v=9ZNBQQegLz8>

Distribuições GNU/Linux - entenda como funciona  
Fórum Internacional Software Livre (FISL) 2014  
http://hemingway.softwarelivre.org/fisl15/high/41e/sala41e-high-201405081704.ogv>

Painel: Governos e software livre - investindo dinheiro público em tecnologias abertas  
Campus Party Brasil (CPBR) 2014  
<https://www.youtube.com/watch?v=eJEXpRkbyNc>

Painel: O processo de construção colaborativo de revistas eletrônicas, fanzines e publicações em geral com software livre  
Campus Party Brasil (CPBR) 2014  
<https://www.youtube.com/watch?v=NoSmeibgnjw>

Painel: Você sabe o quanto é espionado? E como evitar?  
Campus Party Brasil (CPBR) 2014  
<https://www.youtube.com/watch?v=WXi8WjubhKQ>

Painel: É possível empreender e ganhar dinheiro com software livre?  
Campus Party Brasil (CPBR) 2014  
<https://www.youtube.com/watch?v=EBVpJYpXxOc>

Painel: Distribuições GNU/Linux - entenda como funciona  
Campus Party Brasil (CPBR) 2014  
<https://www.youtube.com/watch?v=sfgazgTXhIs>

Painel: neutralidade na rede  
Campus Party Recife (CPRecife) 2013  
<https://www.youtube.com/watch?v=EV4LiFA2ras>

Em defesa do software livre nos cursos de computação das universidades no Brasil  
Fórum Internacional Software Livre (FISL) 2013  
<http://hemingway.softwarelivre.org/fisl14/high/41e/sala41e-high-201307051407.ogg>

Painel: como contribuir em projetos de Software Livre  
Campus Party Brasil (CPBR) 2013  
<https://www.youtube.com/watch?v=K6OLIYU5sOI>

Painel: como a comunidade brasileira contribui com as distribuições GNU/Linux  
Campus Party Recife (CPRecife) 2012  
<https://www.youtube.com/watch?v=JCyVtYoOpmY>
